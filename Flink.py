from e2clab.services import Service
from enoslib.api import actions
from enoslib.objects import Roles


class Flink(Service):

    def deploy(self):
        # registry_opts: Docker registry cache installed on Grid5000.
        # It speed ups your docker-based deployment and also overcomes the docker pull limits.
        registry_opts = dict(type="external", ip="docker-cache.grid5000.fr", port=80)
        self.deploy_docker(registry_opts=registry_opts)

        JOB_MANAGER_REST_PORT = "8081"
        job_manager_service = "job_manager"
        task_manager_service = "task_manager"

        if len(self.hosts) > 1:
            roles_job_manager = Roles({job_manager_service: [self.hosts[0]]})
            roles_task_manager = Roles({task_manager_service: self.hosts[1:len(self.hosts)]})
        else:
            roles_job_manager = Roles({job_manager_service: self.hosts})
            roles_task_manager = Roles({task_manager_service: self.hosts})

        # job_manager
        JOB_MANAGER_ADDRESS = roles_job_manager[job_manager_service][0].address
        self.env.update({"JOB_MANAGER_RPC_ADDRESS": JOB_MANAGER_ADDRESS})

        with actions(roles=roles_job_manager) as p:
            p.docker_container(name=job_manager_service,
                               image=self.service_metadata["image"] if "image" in self.service_metadata else "flink:1.9-scala_2.11",
                               restart="yes",
                               restart_policy="always",
                               network_mode="host",
                               env=self.env,
                               command="jobmanager")
        # task_manager
        with actions(roles=roles_task_manager) as p:
            p.docker_container(name=task_manager_service,
                               image=self.service_metadata["image"] if "image" in self.service_metadata else "flink:1.9-scala_2.11",
                               restart="yes",
                               restart_policy="always",
                               network_mode="host",
                               env=self.env,
                               command="taskmanager")

        # Users may add extra information to Services/sub-Services to access them in "workflow.yaml".
        # e.g.: to access the container name as {{ _self.container_name }} in "workflow.yaml", you can do as follows:
        # Job Managers
        extra_job_managers = []
        for i in range(1, len(roles_job_manager[job_manager_service]) + 1):
            extra_job_managers.append({'container_name': job_manager_service, "my_id": i})
        # Task Managers
        extra_task_managers = []
        for i in range(1, len(roles_task_manager[task_manager_service])+1):
            extra_task_managers.append({'container_name': task_manager_service, "my_id": i})

        # register job_manager Service
        self.register_service(roles=roles_job_manager, service_port=JOB_MANAGER_REST_PORT, sub_service=job_manager_service, extra=extra_job_managers)
        # register task_manager Service
        self.register_service(roles=roles_task_manager, sub_service=task_manager_service, extra=extra_task_managers)

        return self.service_extra_info, self.service_roles
