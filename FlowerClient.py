from e2clab.services import Service
import enoslib as en


class FlowerClient(Service):

    def deploy(self):
        with en.actions(roles=self.roles) as a:
            a.shell("pip3 install mxnet")
            a.shell("pip3 install flwr")

        return self.register_service()
