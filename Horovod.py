from e2clab.services import Service
from enoslib.api import actions, populate_keys
from enoslib.objects import Roles


class Horovod(Service):

    def deploy(self):
        horovod_service = "horovod"

        roles_horovod = Roles({horovod_service: self.hosts})

        # ssh keys for the root users must be generated and pushed to all nodes
        populate_keys(roles_horovod, ".", "id_rsa")

        # https://discuss.pytorch.org/t/issue-with-multiprocessing-semaphore-tracking/22943/4
        self.env.update({
            "PYTHONWARNINGS": "ignore:semaphore_tracker:UserWarning",
            "WANDB_API_KEY": self.service_metadata["wandb_api_key"],
            "WANDB_MODE": self.service_metadata["wandb_mode"],
        })

        extra = []
        if "docker" in self.service_metadata and self.service_metadata["docker"]:
            # registry_opts: Docker registry cache installed on Grid'5000.
            # It speed ups your docker-based deployment and also overcomes the docker pull limits.
            registry_opts = dict(type="external", ip="docker-cache.grid5000.fr", port=80)
            self.deploy_docker(registry_opts=registry_opts)

            if "powerai" in self.service_metadata and self.service_metadata["powerai"]:
                default_image = "ibmcom/powerai:1.7.0-pytorch-ubuntu18.04-py37-ppc64le"
                self.env.update({"LICENSE": "yes"})
            else:
                default_image = "horovod/horovod:master"

            with actions(roles=roles_horovod) as p:
                if "registry_url" in self.service_metadata:
                    p.docker_login(
                        registry_url=self.service_metadata["registry_url"],
                        username=self.service_metadata["registry_username"],
                        password=self.service_metadata["registry_password"]
                    )

                p.docker_container(
                    name="horovod",
                    image=self.service_metadata["image"] if "image" in self.service_metadata else default_image,
                    volumes="/root/.ssh:/root/.ssh",
                    restart="yes",
                    restart_policy="always",
                    network_mode="host",
                    interactive="yes",
                    tty="yes",
                    privileged="yes",
                    env=self.env,
                    device_requests=[{
                        "driver": "nvidia",
                        "count": -1,
                        "capabilities": [["gpu"], ["utility"]]
                    }] if ("gpu" in self.service_metadata and self.service_metadata["gpu"]) else [],
                    command="bash -c '/usr/sbin/sshd -p 12345; sleep infinity'"
                )

            for i in range(0, len(roles_horovod[horovod_service])):
                extra.append({'container_name': horovod_service})
        else:
            if "g5k_pass" in self.service_metadata:
                for i in range(0, len(roles_horovod[horovod_service])):
                    extra.append({
                        'g5k_pass': self.service_metadata["g5k_pass"],
                        'g5k_job_id': self.service_metadata["g5k_job_id"],
                    })

        for i in range(0, len(roles_horovod[horovod_service])):
            extra.append({
                f"horovod{i + 1}": self.hosts[i].address,
            })

        # register node Service
        return self.register_service(roles=roles_horovod, extra=extra)
