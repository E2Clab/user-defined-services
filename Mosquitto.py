from e2clab.services import Service
from enoslib.api import actions


class Mosquitto(Service):
    def deploy(self):
        # registry_opts: Docker registry cache installed on Grid’5000.
        # It speed ups your docker-based deployment and also overcomes the docker pull limits.
        registry_opts = dict(type="external", ip="docker-cache.grid5000.fr", port=80)
        self.deploy_docker(registry_opts=registry_opts)
        with actions(roles=self.roles) as p:
            p.docker_container(name="mosquitto",
                               image="eclipse-mosquitto:latest",
                               restart="yes",
                               restart_policy="always",
                               network_mode="host")
        return self.register_service(service_port="1883")

