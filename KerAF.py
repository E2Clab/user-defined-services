from e2clab.services import Service
from enoslib.api import actions
from enoslib.objects import Roles


class KerAF(Service):

    def deploy(self):
        # registry_opts: Docker registry cache installed on Grid’5000.
        # It speed ups your docker-based deployment and also overcomes the docker pull limits.
        registry_opts = dict(type="external", ip="docker-cache.grid5000.fr", port=80)
        self.deploy_docker(registry_opts=registry_opts)

        # Define sub-services.
        # Will be selected using xxx.kera.1.coordinator/broker/client
        coordinator_service = "coordinator"
        broker_service = "broker"
        job_manager_service = "job_manager"
        task_manager_service = "task_manager"

        if len(self.hosts) > 1:
            roles_coordinator = Roles({coordinator_service: [self.hosts[0]]})
            roles_broker = Roles({broker_service: self.hosts[1:len(self.hosts)]})
            roles_job_manager = Roles({job_manager_service: [self.hosts[0]]})
            roles_task_manager = Roles({task_manager_service: self.hosts[1:len(self.hosts)]})
        else:
            roles_coordinator = Roles({coordinator_service: self.hosts})
            roles_broker = Roles({broker_service: self.hosts})
            roles_job_manager = Roles({job_manager_service: self.hosts})
            roles_task_manager = Roles({task_manager_service: self.hosts})

        # KerA coordinator
        COORDINATOR_ADDRESS = roles_coordinator[coordinator_service][0].address

        # KerFlink job_manager
        JOB_MANAGER_REST_PORT = "8081"
        JOB_MANAGER_ADDRESS = roles_job_manager[job_manager_service][0].address

        self.env.update({"JOB_MANAGER_RPC_ADDRESS": JOB_MANAGER_ADDRESS})
        self.env.update({"FLINK_PROPERTIES": """env.java.opts.taskmanager: -Djava.library.path=/usr/local/lib/
                    jobmanager.memory.process.size: 10gb
                    taskmanager.memory.process.size: 10gb
                    taskmanager.numberOfTaskSlots: 4
                    parallelism.default: 1"""})

        for h in roles_coordinator[coordinator_service]:
            h.extra.update(address=h.address)

        with actions(roles=roles_coordinator) as p:
            p.docker_container(name=coordinator_service,
                               image=self.service_metadata["image"] if "image" in self.service_metadata else "registry.gitlab.inria.fr/kera/kera:0.3.1",
                               restart="yes",
                               restart_policy="always",
                               network_mode="host",
                               ulimits="memlock:-1:-1",
                               env=self.env,
                               command=("coordinator -C tcp:host={{address}},port=11100 --maxCores 16 -n --reset --clusterName test"))

        for h in roles_broker[broker_service]:
            h.extra.update(address=h.address)

        # KerA broker
        with actions(roles=roles_broker) as p:
            p.shell("docker volume create data")
            p.docker_container(name=broker_service,
                               image=self.service_metadata["image"] if "image" in self.service_metadata else "registry.gitlab.inria.fr/kera/kera:0.3.1",
                               restart="yes",
                               restart_policy="always",
                               network_mode="host",
                               shm_size="2g",
                               ulimits="memlock:-1:-1",
                               env=self.env,
                               volumes="data:/tmp",
                               command=("broker -L tcp:host={{address}},port=11101 --totalMasterMemory 80000 -C tcp:host=" + COORDINATOR_ADDRESS + ",port=11100 --cleanerBalancer fixed:50 -D -d --detectFailures 0 -h 1 -f /tmp/storagemaster1 --maxCores 16 --clusterName test -r 0 --masterOnly --numberActiveGroupsPerStreamlet 1 --masterActiveGroupsPerStreamlet 1"))

        # KerFlink job manager
        with actions(roles=roles_job_manager) as p:
            p.docker_container(name=job_manager_service,
                               image=self.service_metadata["image"] if "image" in self.service_metadata else "registry.gitlab.inria.fr/kera/kera/kerflink:0.3.1",
                               restart="yes",
                               restart_policy="always",
                               network_mode="host",
                               env=self.env,
                               command="jobmanager")

        # KerFlink task manager
        with actions(roles=roles_task_manager) as p:
            p.docker_container(name=task_manager_service,
                               image=self.service_metadata["image"] if "image" in self.service_metadata else "registry.gitlab.inria.fr/kera/kera/kerflink:0.3.1",
                               restart="yes",
                               restart_policy="always",
                               network_mode="host",
                               env=self.env,
                               volumes="data:/tmp",
                               command="taskmanager")

        # register job_manager Service
        self.register_service(roles=roles_job_manager, service_port=JOB_MANAGER_REST_PORT, sub_service=job_manager_service)
        # register task_manager Service
        self.register_service(roles=roles_task_manager, sub_service=task_manager_service)

        # register coordinator Service
        self.register_service(roles=roles_coordinator, sub_service=coordinator_service)
        # register broker Service
        self.register_service(roles=roles_broker, sub_service=broker_service)

        return self.service_extra_info, self.service_roles
