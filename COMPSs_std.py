from e2clab.services import Service
from enoslib.api import populate_keys
from enoslib.objects import Roles


class COMPSs_std(Service):
    def deploy(self):
        # ssh keys for the root users must be generated and pushed to all nodes
        populate_keys(Roles({"compss": self.hosts}), ".", "id_rsa")

        # Assign machines to COMPSs Master and Workers
        compss_master = "compss_master"
        compss_worker = "compss_worker"
        roles_compss_master = Roles({compss_master: [self.hosts[0]]})
        roles_compss_worker = Roles(
            {compss_worker: self.hosts[1:len(self.hosts)]})

        workers = [host.alias for host in roles_compss_worker[compss_worker]]

        # Users may add extra information to Services/sub-Services to access them in "workflow.yaml".
        # e.g, to access the container name as {{ _self.container_name }} in "workflow.yaml", you can do as follows:
        extra_compss_master = [{'workers': ','.join(workers)}]  # COMPSs Master

        # Register the Service
        # register COMPSs Master Service
        self.register_service(roles=roles_compss_master,
                              sub_service="master", extra=extra_compss_master)
        # register COMPSs Worker Service
        self.register_service(roles=roles_compss_worker, sub_service="worker")

        return self.service_extra_info, self.service_roles
