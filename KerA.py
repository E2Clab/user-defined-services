from e2clab.services import Service
from enoslib.api import actions
from enoslib.objects import Roles


class KerA(Service):

    def deploy(self):
        # registry_opts: Docker registry cache installed on Grid’5000.
        # It speed ups your docker-based deployment and also overcomes the docker pull limits.
        registry_opts = dict(type="external", ip="docker-cache.grid5000.fr", port=80)
        self.deploy_docker(registry_opts=registry_opts)
        
        # Define sub-services.
        # Will be selected using xxx.kera.1.coordinator/broker/client
        coordinator_service = "coordinator"
        broker_service = "broker"
        client_service = "client"

        if len(self.hosts) > 1:
            roles_coordinator = Roles({coordinator_service: [self.hosts[0]]})
            roles_broker = Roles({broker_service: self.hosts[1:len(self.hosts)]})
        else:
            roles_coordinator = Roles({coordinator_service: self.hosts})
            roles_broker = Roles({broker_service: self.hosts})

        # KerA coordinator
        COORDINATOR_ADDRESS = roles_coordinator[coordinator_service][0].address

        for h in roles_coordinator[coordinator_service]:
            h.extra.update(address=h.address)

        with actions(roles=roles_coordinator) as p:
            p.docker_container(name=coordinator_service,
                               image=self.service_metadata["image"] if "image" in self.service_metadata else "registry.gitlab.inria.fr/kera/kera:0.3.1",
                               restart="yes",
                               restart_policy="always",
                               network_mode="host",
                               ulimits="memlock:-1:-1",
                               env=self.env,
                               command=("coordinator -C tcp:host={{address}},port=11100 --maxCores 16 -n --reset --clusterName test"))

        for h in roles_broker[broker_service]:
            h.extra.update(address=h.address)

        # KerA broker
        with actions(roles=roles_broker) as p:
            p.shell("docker volume create data")
            p.docker_container(name=broker_service,
                               image=self.service_metadata["image"] if "image" in self.service_metadata else "registry.gitlab.inria.fr/kera/kera:0.3.1",
                               restart="yes",
                               restart_policy="always",
                               network_mode="host",
                               shm_size="2g",
                               ulimits="memlock:-1:-1",
                               env=self.env,
                               volumes="data:/tmp",
                               command=("broker -L tcp:host={{address}},port=11101 --totalMasterMemory 80000 -C tcp:host=" + COORDINATOR_ADDRESS + ",port=11100 --cleanerBalancer fixed:50 -D -d --detectFailures 0 -h 1 -f /tmp/storagemaster1 --maxCores 16 --clusterName test -r 0 --masterOnly --numberActiveGroupsPerStreamlet 1 --masterActiveGroupsPerStreamlet 1"))

        # register coordinator Service
        self.register_service(roles=roles_coordinator, sub_service=coordinator_service)
        # register broker Service
        self.register_service(roles=roles_broker, sub_service=broker_service)

        return self.service_extra_info, self.service_roles
