from e2clab.services import Service
import enoslib as en 

class G5K_IPv6(Service):
    """
        A Grid5000' host with an initiated IPv6 address
    """
    def deploy(self):
        # Get an IPv6 address on the G5k host(s)
        en.run("dhclient -6 br0", roles=self.hosts)
        return self.register_service()
