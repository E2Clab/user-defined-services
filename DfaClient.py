from e2clab.services import Service
import enoslib as en


class DfaClient(Service):

    def deploy(self):
        with en.actions(roles=self.roles) as a:
            a.shell("git clone https://gitlab.com/ssvitor/dataflow_analyzer.git")
            a.shell("cp -rf dataflow_analyzer/library/dfa-lib-python . && rm -rf dataflow_analyzer/")
            a.shell("pip3 install -e dfa-lib-python/")

        return self.register_service()
