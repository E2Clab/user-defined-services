from e2clab.services import Service
import enoslib as en


class FlowerServer(Service):

    def deploy(self):
        with en.actions(roles=self.roles) as a:
            a.shell("pip3 install flwr")

        return self.register_service(service_port=5040)
