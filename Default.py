from e2clab.services import Service


class Default(Service):
    """
        A Default E2Clab Service: used if the service name defined in 'layers_services.yaml' is not found in
        'e2clab/e2clab/services/' directory.
    """
    def deploy(self):
        return self.register_service()
