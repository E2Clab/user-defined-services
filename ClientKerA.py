from e2clab.services import Service


class ClientKerA(Service):
    def deploy(self):
        # registry_opts: Docker registry cache installed on Grid’5000.
        # It speed ups your docker-based deployment and also overcomes the docker pull limits.
        registry_opts = dict(type="external", ip="docker-cache.grid5000.fr", port=80)
        self.deploy_nvidia_docker(registry_opts=registry_opts)

        return self.register_service()
