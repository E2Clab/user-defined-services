from e2clab.services import Service
import json
import enoslib as en


class DfaServer(Service):

    def deploy(self):
        registry_opts = dict(type="external", ip="docker-cache.grid5000.fr", port=80)
        self.deploy_docker(registry_opts=registry_opts)

        with en.actions(roles=self.roles) as a:
            docker_daemon = {
                "debug": True,
                "registry-mirrors": ["http://docker-cache.grid5000.fr:80"],
                "insecure-registries": ["http://docker-cache.grid5000.fr:80"],
                "ipv6": True,
                "fixed-cidr-v6": "fe80::/64"
            }
            a.shell(f"echo '{json.dumps(docker_daemon, indent=4)}' > /etc/docker/daemon.json")
            a.shell("systemctl reload docker")
            a.docker_container(
                name="dfserver",
                image="vitorss/dataflow_analyzer",
                restart="yes",
                restart_policy="always",
                network_mode="default",
                interactive="yes",
                tty="yes",
                privileged="yes",
                published_ports="22000:22000",
                default_host_ip="",
                command="bash -c 'cd dfanalyzer/applications/dfanalyzer/ && ./start-dfanalyzer.sh'"
            )

        return self.register_service(service_port=22000)
