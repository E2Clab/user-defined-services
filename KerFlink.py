from e2clab.services import Service
from enoslib.api import actions
from enoslib.objects import Roles


class KerFlink(Service):

    def deploy(self):
        # registry_opts: Docker registry cache installed on Grid’5000.
        # It speed ups your docker-based deployment and also overcomes the docker pull limits.
        registry_opts = dict(type="external", ip="docker-cache.grid5000.fr", port=80)
        self.deploy_docker(registry_opts=registry_opts)

        # Define sub-services.
        # Will be selected using xxx.kerflink.1.coordinator/broker/client
        job_manager_service = "job_manager"
        task_manager_service = "task_manager"

        if len(self.hosts) > 1:
            roles_job_manager = Roles({job_manager_service: [self.hosts[0]]})
            roles_task_manager = Roles({task_manager_service: self.hosts[1:len(self.hosts)]})
        else:
            roles_job_manager = Roles({job_manager_service: self.hosts})
            roles_task_manager = Roles({task_manager_service: self.hosts})

        # KerFlink job_manager
        JOB_MANAGER_REST_PORT = "8081"
        JOB_MANAGER_ADDRESS = roles_job_manager[job_manager_service][0].address

        self.env.update({"JOB_MANAGER_RPC_ADDRESS": JOB_MANAGER_ADDRESS})
        self.env.update({"FLINK_PROPERTIES": """env.java.opts.taskmanager: -Djava.library.path=/usr/local/lib/
                    jobmanager.memory.process.size: 10gb
                    taskmanager.memory.process.size: 10gb
                    taskmanager.numberOfTaskSlots: 4
                    parallelism.default: 1"""})

        with actions(roles=roles_job_manager) as p:
            p.docker_container(name=job_manager_service,
                               image=self.service_metadata["image"] if "image" in self.service_metadata else "registry.gitlab.inria.fr/kera/kera/kerflink:0.3.1",
                               restart="yes",
                               restart_policy="always",
                               network_mode="host",
                               env=self.env,
                               command="jobmanager")

        # KerFlink task_manager
        with actions(roles=roles_task_manager) as p:
            p.shell("docker volume create data")
            p.docker_container(name=task_manager_service,
                               image=self.service_metadata["image"] if "image" in self.service_metadata else "registry.gitlab.inria.fr/kera/kera/kerflink:0.3.1",
                               restart="yes",
                               restart_policy="always",
                               network_mode="host",
                               env=self.env,
                               volumes="data:/tmp",
                               command="taskmanager")

        # register job_manager Service
        self.register_service(roles=roles_job_manager, service_port=JOB_MANAGER_REST_PORT, sub_service=job_manager_service)
        # register task_manager Service
        self.register_service(roles=roles_task_manager, sub_service=task_manager_service)

        return self.service_extra_info, self.service_roles
