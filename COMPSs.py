from e2clab.services import Service
from enoslib.api import populate_keys
from enoslib.objects import Roles
import enoslib as en


class COMPSs(Service):
    def deploy(self):
        DEFAULT_IMAGE_TAG = "3.3.1"
        # ssh keys for the root users must be generated and pushed to all nodes
        populate_keys(Roles({"compss": self.hosts}), ".", "id_rsa")

        # install docker
        registry_opts = dict(type="external", ip="docker-cache.grid5000.fr", port=80)
        self.deploy_docker(registry_opts=registry_opts)

        # Assign machines to COMPSs Master and Workers
        compss_master = "compss_master"
        compss_worker = "compss_worker"
        roles_compss_master = Roles({compss_master: [self.hosts[0]]})
        roles_compss_worker = Roles({compss_worker: self.hosts[1:len(self.hosts)]})

        # Create an overlay network for standalone containers
        # https://docs.docker.com/network/network-tutorial-overlay/#use-an-overlay-network-for-standalone-containers
        overlay_network = "compss-net"
        en.run("docker swarm leave --force", roles=self.hosts, on_error_continue=True)
        cmd_swarm_init = str(en.run("docker swarm init | awk '/docker swarm join --token/'", roles=roles_compss_master)[0].stdout).lstrip()
        en.run(f"{cmd_swarm_init}", roles=roles_compss_worker)
        en.run(f"docker network create --driver=overlay --attachable {overlay_network}", roles=roles_compss_master)

        image_tag = self.service_metadata.get("image_tag", DEFAULT_IMAGE_TAG)
        image = f"compss/compss:{image_tag}"

        # Start COMPSs Master container
        with en.actions(roles=roles_compss_master) as a:
            a.docker_container(
                name=compss_master,
                image=image,
                volumes="/root/.ssh:/root/.ssh",
                restart="yes",
                restart_policy="always",
                network_mode=overlay_network,
                interactive="yes",
                tty="yes",
                privileged="yes",
                default_host_ip="",
            )

        # Start COMPSs Worker containers
        workers = []
        extra_compss_worker = []
        for host in roles_compss_worker[compss_worker]:
            worker_id = f'{compss_worker}_{host.alias}'
            workers.append(worker_id)
            extra_compss_worker.append({'container_name': worker_id})
            with en.actions(roles=host) as a:
                a.docker_container(
                    name=worker_id,
                    image=image,
                    volumes="/root/.ssh:/root/.ssh",
                    restart="yes",
                    restart_policy="always",
                    network_mode=overlay_network,
                    interactive="yes",
                    tty="yes",
                    privileged="yes",
                    published_ports="43001-43002:43001-43002",
                    default_host_ip="",
                )

        # Users may add extra information to Services/sub-Services to access them in "workflow.yaml".
        # e.g, to access the container name as {{ _self.container_name }} in "workflow.yaml", you can do as follows:
        extra_compss_master = [{'container_name': compss_master, 'workers': ','.join(workers)}]  # COMPSs Master

        # Register the Service
        # register COMPSs Master Service
        self.register_service(roles=roles_compss_master, sub_service="master", extra=extra_compss_master)
        # register COMPSs Worker Service
        self.register_service(roles=roles_compss_worker, sub_service="worker", extra=extra_compss_worker)

        return self.service_extra_info, self.service_roles
