from e2clab.services import Service
from enoslib.api import actions
from enoslib.objects import Roles


class Kafka(Service):

    def deploy(self):
        # registry_opts: Docker registry cache installed on Grid’5000.
        # It speed ups your docker-based deployment and also overcomes the docker pull limits.
        registry_opts = dict(type="external", ip="docker-cache.grid5000.fr", port=80)
        self.deploy_docker(registry_opts=registry_opts)

        if len(self.hosts) > 1:
            roles_leader = Roles({"leader": [self.hosts[0]]})
            roles_followers = Roles({"followers": self.hosts[1:len(self.hosts)]})
        else:
            roles_leader = Roles({"leader": self.hosts})
            roles_followers = Roles({"followers": []})

        zookeeper_service = "zookeeper"
        kafka_leader_service = "leader"
        kafka_follower_service = "follower"
        KAFKA_BROKER_ID = 1
        KAFKA_PORT = "9092"
        ZOOKEEPER_CLIENT_PORT = "2181"
        ZOOKEEPER_ADDRESS = roles_leader["leader"][0].address
        ZOOKEEPER_CONNECT = f"{ZOOKEEPER_ADDRESS}:{ZOOKEEPER_CLIENT_PORT}"
        REPLICATION_FACTOR = str(len(roles_followers["followers"]) + 1)

        # Zookeeper + Kafka leader
        self.env.update({"KAFKA_BROKER_ID": str(KAFKA_BROKER_ID),
                         "KAFKA_ZOOKEEPER_CONNECT": ZOOKEEPER_CONNECT,
                         "KAFKA_ADVERTISED_LISTENERS": f"PLAINTEXT://{roles_leader['leader'][0].address}:{KAFKA_PORT}",
                         "KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR": REPLICATION_FACTOR
        })
        with actions(roles=roles_leader) as p:
            p.docker_container(name=zookeeper_service,
                               image="confluentinc/cp-zookeeper",
                               restart="yes",
                               restart_policy="always",
                               network_mode="host",
                               env={"ZOOKEEPER_CLIENT_PORT": ZOOKEEPER_CLIENT_PORT})
            p.docker_container(name=kafka_leader_service,
                               image="confluentinc/cp-kafka",
                               restart="yes",
                               restart_policy="always",
                               network_mode="host",
                               env=self.env)
        # Followers
        for host in roles_followers["followers"]:
            KAFKA_BROKER_ID += 1
            ADVERTISED_LISTENERS = f"PLAINTEXT://{host.address}:{KAFKA_PORT}"
            with actions(roles=Roles({"followers": [host]})) as p:
                p.docker_container(name=kafka_follower_service,
                                   image="confluentinc/cp-kafka",
                                   network_mode="host",
                                   env={
                                       "KAFKA_BROKER_ID": str(KAFKA_BROKER_ID),
                                       "KAFKA_ZOOKEEPER_CONNECT": ZOOKEEPER_CONNECT,
                                       "KAFKA_ADVERTISED_LISTENERS": ADVERTISED_LISTENERS
                                   })
        # register Kafka Leader Service
        self.register_service(roles=roles_leader, service_port=KAFKA_PORT, sub_service=kafka_leader_service)
        # register Zookeeper Service
        self.register_service(roles=roles_leader, service_port=ZOOKEEPER_CLIENT_PORT, sub_service=zookeeper_service)
        # register Kafka Followers Service
        self.register_service(roles=roles_followers, service_port=KAFKA_PORT, sub_service=kafka_follower_service)
        return self.service_extra_info, self.service_roles
